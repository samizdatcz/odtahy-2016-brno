class Col
  (@name, @field, @fieldPerc) ->

data = d3.tsv.parse ig.data.data, (row) ->
  for field in ["odtah", "misto"]
    continue if field == "Přestupek"
    row[field] = parseInt row[field], 10
  row.percent = row.odtah / (row.misto + row.odtah)
  row.asterisk = row.asterisk == "1"
  row
data.sort (a, b) -> b.percent - a.percent
lineHeight = 50
radius = d3.scale.sqrt!
  ..domain [0 d3.max data.map (.misto)]
  ..range [0 lineHeight - 15]
colHeight = d3.scale.linear!

for datum, index in data
  datum.index = index

headerHeight = 50

container = d3.select ig.containers.base
container.append \ul
  ..append \li
    ..attr \class \header
    ..append \div
      ..attr \class "percent"
      ..attr \data-sorter "percent"
      ..html "Podíl odtažených"
    ..append \div
      ..attr \class "total"
      ..attr \data-sorter "misto"
      ..html "Na místě"
    ..append \div
      ..attr \class "total odtah"
      ..attr \data-sorter "odtah"
      ..html "Odtaženo"
    ..append \div
      ..attr \class \sort-tip
      ..html "Kliknutím na záhlaví sloupce<br>můžete záznamy seřadit"

  ..selectAll \li.body .data data .enter!append \li
    ..attr \class \body
    ..style \top -> "#{headerHeight + lineHeight * it.index}px"
    ..append \div
      ..attr \class \percent
      ..html -> "#{ig.utils.formatNumber it.percent * 100} %"
    ..append \span
      ..attr \class \name
      ..html (.prestupek)
      ..filter (.asterisk)
        ..append \span
          ..attr \class \asterisk
          ..html "<br>včetně dopravních přestupků, které se netýkají parkování"
    ..append \div
      ..attr \class \total
      ..style \width -> "#{radius it.misto}px"
      ..style \height -> "#{radius it.misto}px"
      ..append \div
        ..attr \class \count
        ..style \right -> "#{10 + 1.1 * (radius it.misto)}px"
        ..html -> ig.utils.formatNumber it.misto
    ..append \div
      ..attr \class "total odtah"
      ..style \width -> "#{radius it.odtah}px"
      ..style \height -> "#{radius it.odtah}px"
      ..append \div
        ..attr \class \count
        ..style \right -> "#{10 + 1.1 * (radius it.odtah)}px"
        ..html -> ig.utils.formatNumber it.odtah

container.selectAll "[data-sorter]"
  .on \click ->
    d3.event.preventDefault!
    field = @getAttribute \data-sorter
    data.sort (a, b) -> b[field] - a[field]
    for datum, index in data
      datum.index = index
    container.selectAll \li.body
      ..style \top -> "#{headerHeight + lineHeight * it.index}px"
